using UnityEngine;
using System.Collections;


[AddComponentMenu("Camera-Control/Mouse Look")]

public class came : MonoBehaviour
{
	public enum		RotationAxes { MouseXAndY = 0, MouseX = 1, MouseY = 2 }
	public			RotationAxes axes = RotationAxes.MouseXAndY;
	public float	sensitivityX = 15F;
	public float	sensitivityY = 15F;
	public float	minimumX = -360F;
	public float	maximumX = 360F;
	public float	minimumY = -60F;
	public float	maximumY = 60F;
	float rotationY = 0F;


	void Update ()
	{
		float		xAxisValue = Input.GetAxis("Horizontal");
		float		zAxisValue = Input.GetAxis("Vertical");
		
		if (Camera.current != null)
		{
			Camera.current.transform.Translate(new Vector3(xAxisValue * 20, 0.0f, 20 * zAxisValue));
		}

		//Verifier le multiple * Time.deltaTime
		if (Input.GetKey(KeyCode.Q))
			      transform.position += Vector3.up * (50 * Time.deltaTime);
		  else if (Input.GetKey(KeyCode.E))
			        transform.position += -Vector3.up * (50  * Time.deltaTime);

		if (axes == RotationAxes.MouseXAndY)
		{
			float rotationX = transform.localEulerAngles.y + Input.GetAxis("Mouse X") * sensitivityX;

			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);

			transform.localEulerAngles = new Vector3(-rotationY, rotationX, 0);
		}
		else if (axes == RotationAxes.MouseX)
		{
			transform.Rotate(0, Input.GetAxis("Mouse X") * sensitivityX, 0);
		}
		else
		{
			rotationY += Input.GetAxis("Mouse Y") * sensitivityY;
			rotationY = Mathf.Clamp (rotationY, minimumY, maximumY);

			transform.localEulerAngles = new Vector3(-rotationY, transform.localEulerAngles.y, 0);
		}
	}

	void Start ()
	{
		// Make the rigid body not change rotation
		if (GetComponent<Rigidbody>())
			GetComponent<Rigidbody>().freezeRotation = true;
	}

}
