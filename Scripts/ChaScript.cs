﻿using UnityEngine;
using System.Collections;

public class ChaScript : MonoBehaviour
{
    private Animator anim;
	public VirtualJoystick	moveJoystick;
	private Rigidbody		controller;
	public float			terminalRotationSpeed = 25.0f;
	public float			drag = 0.5f;

	public static bool		jump = false;

    void Start()
    {
		controller = GetComponent<Rigidbody>();
		controller.maxAngularVelocity = terminalRotationSpeed;
		controller.drag = drag;
        anim = GetComponent<Animator>();
    }

    void Update()
    {

		Vector3 dir =  Vector3.zero;

		dir.x = Input.GetAxis("Horizontal"); 
		dir.z = Input.GetAxis("Vertical"); 

		if (dir.magnitude > 1)
			dir.Normalize();

		if (moveJoystick.InputDirection != Vector3.zero)
		{
			dir = moveJoystick.InputDirection * 3;
		}

        if (Input.GetKey(KeyCode.W))
        {
			transform.Translate(Vector3.forward * Time.deltaTime * 10);
            anim.SetBool("Run", true);
        }
        else
        {
            anim.SetBool("Run", false);
        }

        if (Input.GetKey(KeyCode.A))
        {
			transform.Translate(Vector3.left * Time.deltaTime * 10);
            anim.SetBool("RunLeft", true);
        }
        else
        {
            anim.SetBool("RunLeft", false);
        }

        if (Input.GetKey(KeyCode.D))
        {
			transform.Translate(Vector3.right * Time.deltaTime * 10);
            anim.SetBool("RunRight", true);
        }
        else
        {
            anim.SetBool("RunRight", false);
        }

        if (Input.GetKey(KeyCode.S))
        {
			transform.Translate(Vector3.back * Time.deltaTime * 10);
            anim.SetBool("RunBack", true);
        }
        else
        {
            anim.SetBool("RunBack", false);
        }

        if (Input.GetKey(KeyCode.Space))
        {
			//transform.Translate(Vector3.forward * Time.deltaTime * 10);
            anim.SetBool("Attack", true);
        }
        else
        {
            anim.SetBool("Attack", false);
        }
    }
}
