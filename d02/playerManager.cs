﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class playerManager : MonoBehaviour
{

	public float				speed = 1.5f;
	//public Vector3				taarget;
	public List<player>		players = new List<player>();

	void					Start ()
	{
	}

	void					FixedUpdate ()
	{
		foreach (player p in players)
		{
			//Debug.Log(p.name + " go = " + p.go);
			//p.instance.target = p.instance.taarget;
			p.transform.position = Vector3.MoveTowards(p.transform.position, p.instance.target, speed * Time.deltaTime);

			if (p.transform.position.x < p.instance.target.x)
				p.transform.localRotation = Quaternion.Euler(0, 0, 0);
			else if (p.transform.position.x > p.instance.target.x)
				p.transform.localRotation = Quaternion.Euler(0, 180, 0);

			if (p.transform.position == p.instance.target)
			{
				//Debug.Log("OK");
				p.instance.go = false;
				p.instance.GetComponent<Animator>().Play("stay");
			}

			if (p.instance.go)
			{
				if (p.instance.target.y > (p.transform.position.y + .4))
				{
					p.GetComponent<Animator>().Play("run_haut");
				}
				else if (p.instance.target.y < (p.transform.position.y - .4))
				{
					p.GetComponent<Animator>().Play("run_bas");
				}
				else
					p.GetComponent<Animator>().Play("run_droit");
			}
		}



	}
}
