﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class player : MonoBehaviour
{
	public float			speed = 1.5f;
	public bool				selected = false;
	public bool				go = false;
	public AudioSource		son;
	public AudioSource		son2;
	public Vector3			target;
	public player			instance {get; set;}

	void					Awake ()
	{
		target = transform.position;
		son = GetComponent<AudioSource>();
		instance = this;
	}

	void					FixedUpdate ()
	{
		if (Input.GetMouseButtonDown(1))
			selected = false;
		if (Input.GetMouseButtonDown(0) && this.selected)
		{
			go = true;
			son.Play();
			target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			target.z = transform.position.z;
		}

		/*
		*/
		transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);

		if (this.transform.position.x < target.x)
			transform.localRotation = Quaternion.Euler(0, 0, 0);
		else if (this.transform.position.x > target.x)
			transform.localRotation = Quaternion.Euler(0, 180, 0);

		if (this.transform.position == target)
		{
			go = false;
			GetComponent<Animator>().Play("stay");
		}

		if (go)
		{
			if (target.y > (this.transform.position.y + .4))
			{
				GetComponent<Animator>().Play("run_haut");
			}
			else if (target.y < (this.transform.position.y - .4))
			{
				GetComponent<Animator>().Play("run_bas");
			}
			else
				GetComponent<Animator>().Play("run_droit");
		}

	}

	public void					OnMouseDown()
	{
		GameObject[]		players;

		players = GameObject.FindGameObjectsWithTag("Player");
		if (Input.GetKey(KeyCode.LeftControl))
		{
			Debug.Log("LeftControl");
			this.selected = true;
		}
		else if (!(Input.GetKey(KeyCode.LeftControl)))
		{
			foreach (GameObject p in players)
			{
				p.GetComponent<player>().selected = false;
			}
			this.selected = true;
		}
		son2.Play();
	}
}
