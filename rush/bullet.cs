﻿using UnityEngine;
using System.Collections;

public class bullet : MonoBehaviour {

	public static Vector3		target;

	void Start ()
	{
		target = transform.position;
	}

	void Update ()
	{
		/*
		   transform.Translate(Input.mousePosition.x * (Time.deltaTime / 20), Input.mousePosition.y * (Time.deltaTime / 20), Input.mousePosition.z * (Time.deltaTime / 20));
		   */

		Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
		mousePos = new Vector3(mousePos.x, mousePos.y, 0);

		Vector3 diff = mousePos - transform.position;
		diff.Normalize();
		float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
		transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);

	//	transform.position -= transform.up * 3 * Time.deltaTime;
		transform.Translate(mousePos/*Vector2.up * 4 * Time.deltaTime*/);

	}
}

