﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class player : MonoBehaviour {

	public GameObject					john;
	public float						speed = 5;
	public Vector3						target;

	void Start ()
	{
		john = GameObject.Find("rambo");
		target = transform.position;
	}

	void Update ()
	{
		if (Input.GetKey ("d"))
			john.transform.Translate(Vector2.right * (5 * Time.deltaTime), Space.World);
		if (Input.GetKey ("a"))
			john.transform.Translate(Vector2.left * (5 * Time.deltaTime), Space.World);
		if (Input.GetKey ("w"))
			john.transform.Translate(Vector2.up * (5 * Time.deltaTime), Space.World);
		if (Input.GetKey ("s"))
			john.transform.Translate(Vector2.down * (5 * Time.deltaTime), Space.World);

		Vector3 pozycja = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		float AngleRad = Mathf.Atan2(pozycja.y - transform.position.y, pozycja.x - transform.position.x);
		float AngleDeg = (180 / Mathf.PI) * AngleRad;
		this.transform.rotation = Quaternion.Euler(0, 0, (AngleDeg + 84));
	}
}
