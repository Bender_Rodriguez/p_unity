﻿using UnityEngine;
using System.Collections;

public class player : MonoBehaviour 
{

	public GameObject				playe;
	public int						speed = 0;
	public int						jumpForce = 4;
	public static Transform			pp;
	public float					velocity = 1;

	void Start () 
	{
		playe = GameObject.Find("thomas");
		GameObject.Find("john").GetComponent<Rigidbody2D>().isKinematic = true;
		GameObject.Find("claire").GetComponent<Rigidbody2D>().isKinematic = true;
		reset();
	}
	
	void Update () 
	{
		GameObject.Find("claire").transform.rotation = Quaternion.AngleAxis(30, Vector3.up);
		GameObject.Find("thomas").transform.rotation = Quaternion.AngleAxis(30, Vector3.up);
		GameObject.Find("john").transform.rotation = Quaternion.AngleAxis(30, Vector3.up);

		choose();
		move();
		if (Input.GetKey(KeyCode.Escape))
			reset();
		if (playe.GetComponent<Rigidbody2D>().velocity.magnitude < 1)
		{
			jump();
		}
		//OnCollisionEnter2D(playe.GetComponent<Collision2D>());

		//Save player position for the camera
		pp = playe.transform;

		//freeze rotation
		playe.GetComponent<Rigidbody2D>().isKinematic = false;
	}

	//Collision
	void	OnCollisionEnter2D(Collision2D collision)
	{
		print("choc");
	}

	//Sauter
	void	jump()
	{

		if (Input.GetKeyDown(KeyCode.Space))
		{
			speed = 100;
			while (speed > 0)
			{
				playe.transform.Translate(Vector2.up * (Time.deltaTime / jumpForce));
				Debug.Log(speed);
				speed--;
			}
		}
	}

	//Choisir le perso
	void	choose()
	{
		if (Input.GetKey(KeyCode.Alpha1))
		{
			jumpForce = 4;
			playe = GameObject.Find("claire");
			GameObject.Find("john").GetComponent<Rigidbody2D>().isKinematic = true;
			GameObject.Find("thomas").GetComponent<Rigidbody2D>().isKinematic = true;
			velocity = 0.5f;
		}
		if (Input.GetKey(KeyCode.Alpha2))
		{
			jumpForce = 2;
			playe = GameObject.Find("thomas");
			GameObject.Find("john").GetComponent<Rigidbody2D>().isKinematic = true;
			GameObject.Find("claire").GetComponent<Rigidbody2D>().isKinematic = true;
			velocity = 1;
		}
		if (Input.GetKey(KeyCode.Alpha3))
		{
			jumpForce = 1;
			playe = GameObject.Find("john");
			GameObject.Find("claire").GetComponent<Rigidbody2D>().isKinematic = true;
			GameObject.Find("thomas").GetComponent<Rigidbody2D>().isKinematic = true;
			velocity = 2;
		}
	}
	
	//Deplacer le perso
	void	move()
	{
		if (Input.GetKey(KeyCode.LeftArrow))
			playe.transform.Translate(Vector2.left *  Time.deltaTime * velocity);
		if (Input.GetKey(KeyCode.RightArrow))
			playe.transform.Translate(Vector2.right * Time.deltaTime * velocity);
	}

	//Reset Game
	void	reset()
	{
			GameObject.Find("john").transform.position = new Vector2(-1.9f, -3.17f);
			GameObject.Find("thomas").transform.position = new Vector2(-1.18f, -3.47f);
			GameObject.Find("claire").transform.position = new Vector2(-3f, -3.17f);
	}
}
