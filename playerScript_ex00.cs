﻿using UnityEngine;
using System.Collections;

public class playerScript_ex00 : MonoBehaviour 
{

	public GameObject				player;
	public int					speed = 10000;

	// Use this for initialization
	void Start () 
	{
		/*
		Physics2D.gravity = new Vector2(0, 1);
		blue = (GameObject)GameObject.Instantiate(blueP, blue_pos, Quaternion.identity);
		jaune = (GameObject)GameObject.Instantiate(jauneP, jaune_pos, Quaternion.identity);
		red = (GameObject)GameObject.Instantiate(redP, red_pos, Quaternion.identity);
		*/
		player = GameObject.Find("Claire");
	}
	

	//public bool[]					who = new bool[]{true, false, false};

	// Update is called once per frame
	void Update () 
	{

		if (Input.GetKey(KeyCode.Alpha1))
			player = GameObject.Find("Claire");
		if (Input.GetKey(KeyCode.Alpha2))
			player = GameObject.Find("Thomas");
		if (Input.GetKey(KeyCode.Alpha3))
			player = GameObject.Find("John");

		//transform.LookAt(player.transform.position);

		if (Input.GetKey(KeyCode.LeftArrow))
			player.transform.Translate(Vector2.left * (5 * Time.deltaTime));

		if (Input.GetKey(KeyCode.RightArrow))
			player.transform.Translate(Vector2.right * (5 * Time.deltaTime));

		if (Input.GetKeyDown(KeyCode.Space))
		{
			speed = 10000;
			while (speed > 0)
			{
				player.transform.Translate(Vector2.right * (5 * Time.deltaTime));
				speed--;
			}
		}

		/*

		if (Input.GetKey(KeyCode.R))
		{
			GameObject.Destroy(red);
		}
		*/
	}
}
