﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
	public float				speed;

	void Start ()
	{speed = Time.deltaTime * 9;}
	
	void Update ()
	{
		if (Input.GetKey(KeyCode.W) && this.transform.position.y < 5)
			transform.Translate(Vector2.up * speed);
		if (Input.GetKey(KeyCode.S) && this.transform.position.y > -5)
			transform.Translate(Vector2.down * speed);
	}
}
