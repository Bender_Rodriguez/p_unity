﻿using UnityEngine;
using System.Collections;

public class Cube : MonoBehaviour
{
	/*
	// Use this for initialization
	void Start () {}
	*/

	public int			speed = Random.Range(2, 7);
	
	// Update is called once per frame
	void Update ()
	{
		float			time = speed * Time.deltaTime;
		transform.Translate(0, -time, 0);
	}
}
