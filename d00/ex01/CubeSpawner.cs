﻿using UnityEngine;
using System.Collections;

public class CubeSpawner : MonoBehaviour
{

	/*
	// Use this for initialization
	void Start () {}
	*/
	
	public GameObject		A;
	public GameObject		S;
	public GameObject		D;
	public GameObject		letter;

	public GameObject		Aprefab;
	public GameObject		Sprefab;
	public GameObject		Dprefab;

	public  bool[] 			check = new bool[]{false, false, false};
	public int				nb;
	public int				rand_nb;
	public float			time = 0;
	public float			start = 4;

	// Update is called once per frame
	void Update ()
	{
		rand_nb = Random.Range(0, 3);
		if (rand_nb == 0 && check[rand_nb] == false)
		{
			Vector2 pos = new Vector2(-2, start);
			A = (GameObject)GameObject.Instantiate(Aprefab, pos, Quaternion.identity);
			check[0] = true;
		}
		else if (rand_nb == 1 && check[rand_nb] == false)
		{
			Vector2 pos1 = new Vector2(0, start);
			S = (GameObject)GameObject.Instantiate(Sprefab, pos1, Quaternion.identity);
			check[1] = true;
		}
		else if (rand_nb == 2 && check[rand_nb] == false)
		{
			Vector2 pos2 = new Vector2(2, start);
			D = (GameObject)GameObject.Instantiate(Dprefab, pos2, Quaternion.identity);
			check[2] = true;
		}
		if (rand_nb == 0)
			letter = A;
		if (rand_nb == 1)
			letter = S;
		if (rand_nb == 2)
			letter = D;
		if ((rand_nb == 0 && Input.GetKeyDown(KeyCode.A))
				|| (rand_nb == 1 && Input.GetKeyDown(KeyCode.S))
				|| (rand_nb == 2 && Input.GetKeyDown(KeyCode.D)))
		{
			GameObject.Destroy(letter);
			check[rand_nb] = false;
			Debug.Log("Precision: " + (letter.transform.localPosition.y + 4));
		}
		else if (letter.transform.localPosition.y < -4.5)
		{
			GameObject.Destroy(letter);
			check[rand_nb] = false;
			Debug.Log("Precision: You miss");
		}

	}
}
