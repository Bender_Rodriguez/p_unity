﻿using UnityEngine;
using System.Collections;

public class Ball : MonoBehaviour
{
	/*
	// Use this for initialization
	void Start () {}
	*/

	public float		speed;
	public GameObject	boule;
	public GameObject	Pball;
	public static bool	going = false;
	public bool			up = true;
	public static float	pos;

	// Update is called once per frame
	void Update ()
	{
		if (going == false)
			speed = Club.power * Time.deltaTime;
		if (this.transform.localPosition.y < 2.6f
				&& this.transform.localPosition.y > 2.3f && speed < 0.08f)
		{
			Club.hit = false;
			Club.win = true;
			if (Club.score == 0 || Club.score == -5
					|| Club.score == -10 || Club.score == -15)
			{
				Debug.Log("Congratulations!! your score is " + Club.score);
			}
			else
				Debug.Log("You loose... your score is " + Club.score);
			GameObject.Destroy(this);
		}
		if (Club.hit == true && up && this.transform.localPosition.y < 3.75f)
		{
			going = true;
			if (speed > 0)
				transform.Translate(Vector2.up * speed);
			else
			{
				pos = this.transform.localPosition.y;
				Club.hit = false;
				Club.power = 0;
				going = false;
			}
			speed = speed - 0.01f;
		}
		else if ((Club.hit == true && up && this.transform.localPosition.y >= 3.75f)
				|| !up)
		{
			up = false;
			if (speed > 0)
				transform.Translate(Vector2.down * speed);
			else
			{
				up = true;
				pos = this.transform.localPosition.y;
				Club.power = 0;
				Club.hit = false;
				going = false;
			}
			if (this.transform.localPosition.y <= -3.75f)
				up = true;
			speed = speed - 0.01f;
		}
	}
}
