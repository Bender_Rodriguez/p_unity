﻿using UnityEngine;
using System.Collections;

public class Club : MonoBehaviour
{
	/*
	// Use this for initialization
	void Start () {}
	*/
	
	public static int			score = -15;
	public GameObject			baton;
	public GameObject			PreClub;
	public GameObject			Pclub;
	public float				direction;
	public static bool			win = false;
	public static int			power = 0;
	public bool					check_destroy = false;
	public static bool			hit = false;
	public Vector2				temp;
	public static Vector2		pos = new Vector2(-0.3f, -2f);

	// Update is called once per frame
	void Update ()
	{
		baton = (GameObject)GameObject.Instantiate(PreClub, pos, Quaternion.identity);
		GameObject.Destroy(baton);
		if (Input.GetKey(KeyCode.Space) && win == false)
		{
			temp = pos;
			check_destroy = true;
			transform.Translate(Vector2.down * Time.deltaTime);
			GameObject.Destroy(Pclub);
			power++;
		}
		else if (check_destroy)
		{
			check_destroy = false;
			if (win == false)
				score += 5;
			hit = true;
			/*
			if (Ball.going == false)
				transform.position = new Vector3(0, Ball.pos, 0);
				*/
		}
		if (Ball.going == false && hit == false && power == 0 && score > -15)
			transform.position = new Vector3(0, Ball.pos, 0);
		/*
		if (win == true)
			GameObject.Destroy(this);
			*/
		Debug.Log("Score: " + score);
	}
}
