﻿using UnityEngine;
using System.Collections;

public class baloon: MonoBehaviour {

	float			timeLeft = 0;
	Vector2			scale;
	float			taille;
	bool			burst = false;
	int				souffle = 5000;

	/*
	// Use this for initialization
	void Start ()
	{
	}
	*/
	
	// Update is called once per frame
	void Update ()
	{
		Debug.Log("Souffle restant: " + souffle);
		taille = transform.localScale.x - (float)0.05;
		transform.localScale = new Vector2(taille, taille);

		scale = transform.localScale;
		timeLeft += Time.deltaTime;
		if (Input.GetKeyDown(KeyCode.Space) && !burst && souffle > 0)
		{
			souffle -= 100;
			taille = transform.localScale.x + (float)1.0;
			transform.localScale = new Vector2(taille, taille);
			burst = scale.x > 8 ? true : false;
			Debug.Log(scale);
		}
		if (souffle < 5000)
			souffle++;
		if (burst)
		{
			while (taille > 0.5)
			{
				taille = transform.localScale.x - (float)0.05;
				transform.localScale = new Vector2(taille, taille);
			}
		}
		if (scale.x < 0.1)
		{
			Debug.Log("Balloon life time: " + Mathf.RoundToInt(timeLeft) + "s");
			GameObject.Destroy(this);
		}
	}
}
