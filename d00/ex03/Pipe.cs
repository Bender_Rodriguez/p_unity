﻿using UnityEngine;
using System.Collections;

public class Pipe : MonoBehaviour {

	public GameObject			Pbarre;
	public static GameObject	barre;
	public Vector2				start = new Vector2(5, 1);
	public static bool			going = false;
	public float				speed;
	public float				score = 0;

	void Start ()
	{
		speed = Time.deltaTime * 3;
	}

	
	void Update ()
	{

		if (going == false)
		{
			barre = (GameObject)GameObject.Instantiate(Pbarre, start, Quaternion.identity);
			going = true;
		}
		if (going == true)
			barre.transform.Translate(Vector2.left * speed);
		if (barre.transform.position.x < -6.2)
		{
			score += 5;
			GameObject.Destroy(barre);
			going = false;
		}
	}
}
