﻿using UnityEngine;
using System.Collections;

public class Bird : MonoBehaviour {

	public bool					up = true;
	public bool					jump = false;
	public float				jumpPos;

	void Start ()
	{
	}

	
	void Update ()
	{
		if (this.transform.position.y < -2.6)
		{
			Debug.Log("PERDU");
			Application.Quit();
		}
		if (!jump && this.transform.position.y > -2.6)
			transform.Translate(Vector2.down * (8 * Time.deltaTime));
		else
		{
			if (up)
			{
				transform.Translate(Vector2.up * (18 * Time.deltaTime));
				if (this.transform.position.y > (jumpPos + 1.8))
				{
					jump = false;
					up = false;
				}
			}
		}
		if (Input.GetKeyDown(KeyCode.Space))
		{
			jump = true;
			up = true;
			jumpPos = this.transform.position.y;
		}
	}
}
